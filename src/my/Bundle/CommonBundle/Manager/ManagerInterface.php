<?php
/**
 * Description of ManagerInterface.php.
 *
 * @package my\Bundle\CommonBundle\Manager
 * @author  Joelio
 */

namespace my\Bundle\CommonBundle\Manager;


interface ManagerInterface
{

    /**
     * @param \Symfony\Component\Form\Form $form
     * @return mixed
     */
    public function setForm(\Symfony\Component\Form\Form $form);

    /**
     * @param $entity
     * @return mixed
     */
    public function setEntity($entity);

}