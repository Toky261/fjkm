<?php
/**
 * Description of BaseManager.php.
 *
 * @package my\Bundle\CommonBundle\Manager
 * @author  Joelio
 */

namespace my\Bundle\CommonBundle\Manager;


/**
 * Class BaseManager
 *
 * @package AppBundle\Manager
 */
abstract class BaseManager implements ManagerInterface
{

    /**
     * @var \Symfony\Component\Form\Form
     */
    protected $form;

    /**
     * @var mixed
     */
    protected $entity;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * @param \Symfony\Component\Form\Form $form
     *
     * @return $this
     */
    public function setForm(\Symfony\Component\Form\Form $form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Set entity
     *
     * @param mixed $entity
     *
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return $this
     */
    public function save()
    {

        if ($this->getCustomEntity()) {
            if (!$this->entity->getId()) { // new
                $this->em->persist($this->entity);
            }

            $this->em->flush($this->entity);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function delete()
    {
        $oEntity = $this->getCustomEntity();
        if ($oEntity) {
            $this->em->remove($oEntity);

            $this->em->flush($oEntity);

            return true;

        }

        return false;
    }

    /**
     * @return mixed|\Symfony\Component\Form\Form
     */
    public function getCustomEntity()
    {
        return !empty($this->entity) ? $this->entity : $this->form;
    }

}