<?php

namespace my\Bundle\CommonBundle\Command;

use my\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand as Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ImportCiviliteCommand extends Command
{
	/**
	 * Configure.
	 */
	protected function configure()
	{
		$this
			->setName('import:civilite')
			->setDefinition(array(
				                new InputArgument('civilite', InputArgument::REQUIRED, 'The civility'),
			                ))
			->setDescription('Import candidats')
			->setHelp(<<<EOT
The <info>%command.name%</info>Import candidats
EOT
			)
		;
	}

	/**
	 * @param \Symfony\Component\Console\Input\InputInterface   $input
	 * @param \Symfony\Component\Console\Output\OutputInterface $output
	 *
	 * @return void
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try {
			$output->writeln('Imports Start');

			$kernel     = $this->getContainer()->get('kernel');
			$oCsvParser = $this->getContainer()->get('my\Bundle\CommonBundle\Services\CsvParserService');

			$zSexe = 'Homme';
			$avatar = 'default_homme.png';
			$zFile = $kernel->getRootDir() . '/../oneshot/candidats-7-fevrier_homme.csv';
			if ($input->getArgument('civilite') == 'femme') {
				$zSexe = 'Femme';
				$avatar = 'default_femme.png';
				$zFile = $kernel->getRootDir() . '/../oneshot/candidats-7-fevrier_femme.csv';
			}
			if (file_exists($zFile)) {
				$oCsvParser->delimiter = ';';
				$csvs                  = $oCsvParser->parse_file($zFile);

				/** @var \Doctrine\Common\Persistence\ManagerRegistry $oEm */
				$oEm      = $this->getContainer()->get('doctrine.orm.default_entity_manager');
				$doctrine = $this->getContainer()->get('doctrine');

				// nb candidature importés
				$nbImportedCandidatures = 0;
				foreach ($csvs as $k => $items) {

					/** @var User $oUser */
					$oUser = $doctrine->getRepository(User::class)->findOneByNewRegistryNumber((int) $items['new_registry_number']);
					if ($oUser) {
						$oUser->setSexe($zSexe);
						$oUser->setImage($avatar);

						$oEm->flush($oUser);

						dump($items);

						// incremente nb offre importés
						$nbImportedCandidatures ++;
					}
				}

				$output->writeln($nbImportedCandidatures . ' Imports Finished');
			}


		} catch (\Exception $e) {

			$output->writeln('Imports with error in "' . $e->getMessage());
		}
	}

	/**
	 * {@inheritdoc}
	 */
	protected function interact(InputInterface $input, OutputInterface $output)
	{
		$questions = array();

		if (!$input->getArgument('civilite')) {
			$question = new Question('Civilité à importer ("homme" ou "femme") :');
			$question->setValidator(function ($firstname) {
				if (empty($firstname)) {
					throw new \Exception('Civilité est obligatoire');
				}

				return $firstname;
			});
			$questions['civilite'] = $question;
		}

		foreach ($questions as $name => $question) {
			$answer = $this->getHelper('question')->ask($input, $output, $question);
			$input->setArgument($name, $answer);
		}
	}
}