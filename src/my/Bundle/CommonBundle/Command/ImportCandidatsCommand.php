<?php

namespace my\Bundle\CommonBundle\Command;

use my\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand as Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCandidatsCommand extends Command
{
    /**
     * Configure.
     */
    protected function configure ()
    {
        $this
            ->setName('import:candidats')
            ->addOption(
                'force',
                null,
                InputOption::VALUE_REQUIRED,
                'Do you want to force import ?',
                0
            )
            ->setDescription('Import candidats')
            ->setHelp(<<<EOT
The <info>%command.name%</info>Import candidats
EOT
            );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute (InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln('Imports Start');

	        $kernel = $this->getContainer()->get( 'kernel');
	        $oCsvParser = $this->getContainer()->get( 'my\Bundle\CommonBundle\Services\CsvParserService');

	        $zFile = $kernel->getRootDir() . '/../oneshot/candidats-7-fevrier_lahy_vavy.csv';
	        if (file_exists($zFile)) {
		        $oCsvParser->delimiter = ';';
		        $csvs = $oCsvParser->parse_file($zFile);

		        /** @var \Doctrine\Common\Persistence\ManagerRegistry $oEm */
		        $oEm = $this->getContainer()->get('doctrine.orm.default_entity_manager');
                        $doctrine = $this->getContainer()->get('doctrine');

		        // nb candidature importés
		        $nbImportedCandidatures = 0;
		        foreach ($csvs as $k => $items) {
                            
                            $user = $doctrine->getRepository(User::class)->findOneByNewRegistryNumber((int)$items['new_registry_number']);
                            dump((int)$items['new_registry_number'] . '--' . $user->getId());
                            $dateNow = new \DateTime();
                            $candidature = new \my\Bundle\CommonBundle\Entity\Candidature();
                            $candidature->setUser($user);
                            $candidature->setCreated($dateNow);
                            $candidature->setUpdated($dateNow);
                            $candidature->setStatus(1);

                            $oEm->persist($candidature);
                            $oEm->flush();

                            // incremente nb offre importés
                            $nbImportedCandidatures++;
		        }

		        $output->writeln($nbImportedCandidatures . ' Imports Finished');
	        }


        } catch ( \Exception $e ) {

            $output->writeln('Imports with error in "' . $e->getMessage());
        }
    }
}