<?php

namespace my\Bundle\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class ToolsClearTableCommand extends ContainerAwareCommand
{
	/**
	 * Configure.
	 */
	protected function configure()
	{
		$this
			->setName('app:clear:table')
			->setDescription('Clear specified tables')
			->setHelp('This command clears specified tables with this command => php bin/console app:clear:table -t "tableA, tableB, tableC ..." [boolean -c Cascade]')
			->setDefinition(
                new InputDefinition([
					new InputOption('tables', 't', InputOption::VALUE_REQUIRED, "tables"),
					new InputOption('cascade', 'c', InputOption::VALUE_OPTIONAL, "cascade"),
                ])
            )
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try {
			$io = new SymfonyStyle($input, $output);

			$output->writeln("\nProcess started");

			$tables 	= explode(',', $input->getOption('tables'));
			$cascade 	= false;
			$tableResult= [];
			$hasError	= false;

			if (!empty($tables)) {
				$em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
				$dbConnection = $em->getConnection();
				$platform = $dbConnection->getDatabasePlatform();
				$dbConnection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');

				foreach ($tables as $table) {
					$table = trim($table);
					try {
						$output->writeln("\nClearing " . $table);
						$dbConnection->executeUpdate($platform->getTruncateTableSQL($table, $cascade));
						$tableResult[] = [$table, 'Cleaned'];
					} catch (\Exception $e) {
						$tableResult[] = [$table, 'Error'];
						$io->warning('Error cleaned data from \'' . $table . '\'. Error: ' . $e->getMessage());
						$dbConnection->query('SET FOREIGN_KEY_CHECKS=1;');
						throw $e;
					}
				}

				$dbConnection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');

				$message = ($hasError == false) ? "Great!!! These tables were cleaned" : "Some errors was occured";

				$io->success($message);
				$io->table(['Table', 'Status'], $tableResult);
			}
			else {
				$io->warning("Sorry, you must at least specify a table");
			}

		} catch (\Exception $e) {
			$io->warning('Process error: "' . $e->getMessage());
		}
	}
}