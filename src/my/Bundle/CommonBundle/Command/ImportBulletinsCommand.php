<?php

namespace my\Bundle\CommonBundle\Command;

use my\Bundle\UserBundle\Entity\User;
use my\Bundle\CommonBundle\Entity\Bulletin;
use my\Bundle\CommonBundle\Services\ImageResizer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand as Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportBulletinsCommand extends Command
{
	/**
	 * Configure.
	 */
	protected function configure()
	{
		$this
			->setName('import:bulletins')
			->setDescription('Import "Bulletin"')
			->setHelp('This command imports "bulletin" with this command => php bin/console import:bulletins')
			->setDefinition([
				new InputArgument('civilite', InputArgument::REQUIRED, 'The civility'),
				new InputArgument('bulletinNum', InputArgument::REQUIRED, 'Numero du premier bulletin'),
				new InputArgument('page', InputArgument::REQUIRED, "Nombre de page d'un bulletin"),
			])
		;
	}

/**
	 * @param \Symfony\Component\Console\Input\InputInterface   $input
	 * @param \Symfony\Component\Console\Output\OutputInterface $output
	 *
	 * @return void
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try {
			$io = new SymfonyStyle($input, $output);
			$importStart = new \DateTime();
			$output->writeln("\nImports start at ") . $importStart->format('d-m-Y G:i:s');

			$civilite = $input->getArgument('civilite');
			$bulletinNum = intval($input->getArgument('bulletinNum'));
			$nbPageBulletin = intval($input->getArgument('page'));

			$kernel = $this->getContainer()->get('kernel');
			$path   = $kernel->getRootDir() . '/../oneshot/bulletins/' . $civilite . '/';
			$files  = $this->listeContent($path);

			$images = [];
			$depart = 0;

			// Ballot number with format 00XXXX
			$formatSize = 6;

			// creation repertoire de destination si necessaire afin d'eviter erreur
			if(!file_exists($kernel->getRootDir() . '/../web/bulletins/')){
				mkdir($kernel->getRootDir() . '/../web/bulletins/', 0777, true);
			}

			// Define the size of record, the frequency for persisting the data and the current index of records
			$bulletinFileSize = count($files);
			$batchSize = 20;
			$i = 1;	

			// Starting progress
			$progress = new ProgressBar($output, $bulletinFileSize);
			$progress->start();

			foreach ($files as $key => $file) {
				$resizer = new ImageResizer();
				$numero = $depart + $key + 1;

				$pathFile = $path . $file;

				$newFile = $civilite . '_' . $numero . '.jpeg';

				$pathDestinationFile = $kernel->getRootDir() . '/../web/bulletins/' . $newFile;
				// rename($pathFile, $pathDestinationFile);
				
				$resizer->resizeImage($pathFile, $pathDestinationFile , 560, 792);

				$images[] = 'bulletins/' . $newFile;
				
				$nonEnregistrement = $key % $nbPageBulletin;
				
				if ($nonEnregistrement == ($nbPageBulletin -1)) {
					// recuperation dernier numero dans la table bulletin
					$lastNumber = $this->getLastNumber();

					$newNumber = $this->concatNumber($formatSize, $bulletinNum);
					
					$dateNow  = new \DateTime();
					$bulletin = new Bulletin();
					$bulletin->setImage(json_encode($images));
					$bulletin->setContent('');
					$bulletin->setNumero($newNumber);
					$bulletin->setSexe((strtolower($civilite) != 'homme') ? 'Femme' : ucfirst($civilite));
					$bulletin->setCreated($dateNow);
					$bulletin->setUpdated($dateNow);
					$bulletin->setStatus(Bulletin::STATUS_INIT);

					/** @var \Doctrine\Common\Persistence\ManagerRegistry $oEm */
					$oEm = $this->getContainer()->get('doctrine.orm.default_entity_manager');
					$oEm->persist($bulletin);
					$oEm->flush();
					
					$images = [];
					$bulletinNum ++;
				}

				$progress->advance();				
				$now = new \DateTime();
				$output->writeln(' of bulletins resized and imported [' . $newFile . '] ... | ' . $now->format('d-m-Y G:i:s'));

				$i++;
			}
			
			$progress->finish();

			$io->success('Great!!! ' . $bulletinFileSize . ' Bulletins imported');

		} catch (\Exception $e) {
			$io->warning('Imports with error "' . $e->getMessage());
		}
	}

	/**
	 * {@inheritdoc}
	 */
	protected function interact(InputInterface $input, OutputInterface $output)
	{
		$questions = [];

		if (!$input->getArgument('civilite')) {
			$question = new Question('Civilité à importer ("homme" ou "femme") : ');
			$question->setValidator(function ($civility) {
				if (empty($civility)) {
					throw new \Exception('Civilité est obligatoire');
				}

				return $civility;
			});
			$questions['civilite'] = $question;
		}

		if (!$input->getArgument('bulletinNum')) {
			$question = new Question('Quelle est le numéro du premier bulletin? : ');
			$question->setValidator(function ($bulletinNum) {
				if (empty($bulletinNum)) {
					throw new \Exception('Le numéro de départ est obligatoire');
				}

				return $bulletinNum;
			});
			$questions['bulletinNum'] = $question;
		}

		if (!$input->getArgument('page')) {
			$question = new Question('Combien de page contient-il un bulletin? : ');
			$question->setValidator(function ($page) {
				if (empty($page)) {
					throw new \Exception('Le nombre de page du bulletin est obligatoire');
				}

				return $page;
			});
			$questions['page'] = $question;
		}

		foreach ($questions as $name => $question) {
			$answer = $this->getHelper('question')->ask($input, $output, $question);
			$input->setArgument($name, $answer);
		}
	}

	private function listeContent($path)
	{
            //
		
            $listeContents     = [];
            $extensionImages[] = 'jpeg';
            $extensionImages[] = 'jpg';
//              $extensionImages[] = 'gif';
//		$extensionImages[] = 'png';
            $dossier = opendir($path);

            if (!$dossier) {
                
                return $listeContents;
            }
                    

            while (false !== ($file = readdir($dossier))) {
                if ($file != '.' && $file != '..' && $file != 'index.php' && !is_dir($file)) {
                    $pathFile  = $path . $file;
                    $extension = pathinfo($pathFile, PATHINFO_EXTENSION);
                    if (in_array(strtolower($extension), $extensionImages)) {
                            $listeContents[] = $file;
                    }
                }
            }
            
            // dump($listeContents);
//            exit();

            return $listeContents;
	}

	private function getExtension($path)
	{
		$listeContents = [];
		$dossier       = opendir($path);
		if (!$dossier)
			return $listeContents;

		while (false !== ($file = readdir($dossier))) {
			pathinfo();
			if ($file != '.' && $file != '..' && $file != 'index.php' && !is_dir($file)) {
				$listeContents[] = $file;
			}
		}

		return $listeContents;
	}

	/**
	 * @return bool|string
	 * @throws \Doctrine\DBAL\DBALException
	 */
	private function getLastNumber()
	{
		$em = $this->getContainer()->get('doctrine.orm.entity_manager');
		$qb = $em
		           ->getConnection()
		           ->prepare("
                            SELECT
                                IF(`numero` IS NULL, 1, MAX(`numero`) + 1) AS num
                            FROM
                                `bulletin`
                            GROUP BY numero
                    ");

		$qb->bindParam('userId', $userId, \PDO::PARAM_INT);
		$qb->execute();

		return $qb->fetchColumn();
	}

	private function concatNumber($formatSize, $currentNum) {
        $zero = $formatSize - strlen($currentNum);
        $zeros = "";
        for ($i=0; $i < $zero; $i++) {
            $zeros = $zeros . "0";
        }
        return $zeros . $currentNum;
    }
}