<?php

namespace my\Bundle\CommonBundle\Command;

use my\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand as Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use my\Bundle\CommonBundle\Services\CsvParserService;

class ImportMembersCommand extends Command
{
    /**
     * Configure.
     */
    protected function configure ()
    {
        $this
            ->setName('import:members')
            ->addOption(
                'force',
                null,
                InputOption::VALUE_REQUIRED,
                'Do you want to force import ?',
                0
            )
            ->setDescription('Import Members')
            ->setHelp(<<<EOT
The <info>%command.name%</info>Import Offers into Products
EOT
            );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute (InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln('Imports Start');

	        $kernel = $this->getContainer()->get( 'kernel');
	        $oCsvParser = $this->getContainer()->get( 'my\Bundle\CommonBundle\Services\CsvParserService');
                
                /* @var $oCsvParser CsvParserService */
                $oCsvParser->encoding(NULL, 'utf-8');

//	        $zFile = $kernel->getRootDir() . '/../oneshot/mpandray.csv';
	        $zFile = $kernel->getRootDir() . '/../oneshot/mpandray-2019-02-14.csv';
	        if (file_exists($zFile)) {
		        $oCsvParser->delimiter = ';';
		        $tCsv = $oCsvParser->parse_file($zFile);

		        /** @var \Doctrine\Common\Persistence\ManagerRegistry $oEm */
		        $oEm = $this->getContainer()->get('doctrine.orm.default_entity_manager');
		        $oEncoder = $this->getContainer()->get('security.password_encoder');
                        $doctrine = $this->getContainer()->get('doctrine');

		        // nb offres importés
		        $nbImportedOffers = 0;
		        foreach ($tCsv as $k => $items) {
                        dump($items);
                                    $names = explode( ' ', $items['ANARANA']);
                                    $zUsername = strtolower($items["LAH."] . '_' . $names[0]);

                                    $user = $doctrine->getRepository(User::class)->findOneByNewRegistryNumber((int)$items['LAH.']);

                                    $oUser = $user ? $user : new User();

                                    $oUser->setUsername($zUsername);
                                    $oUser->setUsernameCanonical($zUsername);
                                    $oUser->setEmail($zUsername . '@mail.fjkm');
                                    $oUser->setEmailCanonical($zUsername . '@mail.fjkm');
                                    $oUser->setEnabled(1);
                                    $oUser->setPassword($oEncoder->encodePassword($oUser, $zUsername));
                                    $oUser->setRoles( ['ROLE_USER'] );
                                    $oUser->setLastName( $names[0]);

                                    unset( $names[0]);
                                    $zFirstname = implode( ' ', $names);

                                    $oUser->setFirstName( $zFirstname);
                                    $oUser->setPhone( $items['TEL']);
                                    $oUser->setAdresse( $items['ADIRESY']);
                                    $oUser->setSector( $items['FARITRA']);
                                    $oUser->setUserType( User::USER_TYPE_EGLISE);

                                    $oUser->setNewRegistryNumber($items["LAH."]);
                                    $oUser->setStatus(1);

                                    $oEm->persist($oUser);
                                    $oEm->flush($oUser);

                                    // incremente nb offre importés
                                    $nbImportedOffers++;
			        
		        }

		        $output->writeln($nbImportedOffers . ' Imports Finished');
	        }


        } catch ( \Exception $e ) {

            $output->writeln('Imports with error in "' . $e->getMessage());
        }
    }
}