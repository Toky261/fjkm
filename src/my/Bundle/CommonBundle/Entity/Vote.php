<?php

namespace my\Bundle\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use my\Bundle\CommonBundle\Entity\Candidature;
use my\Bundle\CommonBundle\Entity\Bulletin;

/**
 * Vote
 *
 * @ORM\Table(name="vote")
 * @ORM\Entity(repositoryClass="my\Bundle\CommonBundle\Repository\VoteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Vote
{

	/**
	 * @var int
	 */
	const CONST_STATUS_ENABLED = 1;

	/**
	 * @var int
	 */
	const CONST_STATUS_DISABLED = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;
        
    /**
     * @ORM\ManyToOne(targetEntity="\my\Bundle\CommonBundle\Entity\Candidature", inversedBy="votes")
     * @ORM\JoinColumn(name="candidature_id", referencedColumnName="id")
     */
    private $candidature;
        
    /**
     * @ORM\ManyToOne(targetEntity="\my\Bundle\CommonBundle\Entity\Bulletin", inversedBy="votes")
     * @ORM\JoinColumn(name="bulletin_id", referencedColumnName="id")
     */
    private $bulletin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Vote
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Vote
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Vote
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Get candidature
     *
     * @return Candidature
     */
    function getCandidature() {
        return $this->candidature;
    }

    /**
     * Get bulletin
     *
     * @return Bulletin
     */
    function getBulletin() {
        return $this->bulletin;
    }

    /**
     * Set status
     *
     * @param Candidature $candidature
     *
     * @return Vote
     */
    function setCandidature($candidature) {
        $this->candidature = $candidature;
        return $this;
    }

    /**
     * Set status
     *
     * @param Bulletin $bulletin
     *
     * @return Vote
     */
    function setBulletin($bulletin) {
        $this->bulletin = $bulletin;
        return $this;
    }

	/**
	 * @ORM\PrePersist()
	 */
	public function prePersistEvent()
	{
		$this->setCreatedValue();
		$this->setUpdatedValue();
	}

	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdateEvent()
	{
		$this->setUpdatedValue();
	}

	private function setCreatedValue()
	{
		if (!$this->created) {
			$this->created = new \DateTime();
		}
	}

	private function setUpdatedValue()
	{
		if (!$this->updated) {
			$this->updated = new \DateTime();
		}
	}


}

