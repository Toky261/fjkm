<?php

namespace my\Bundle\CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use my\Bundle\CommonBundle\Repository\BulletinRepository;
use my\Bundle\UserBundle\Entity\User;

/**
 * Bulletin
 *
 * @ORM\Table(name="bulletin")
 * @ORM\Entity(repositoryClass="\my\Bundle\CommonBundle\Repository\BulletinRepository")
 */
class Bulletin
{
    
    CONST STATUS_INIT     = 0;
    CONST STATUS_ATTRIBUE = 1;
    CONST STATUS_AVALIDER = 2;
    CONST STATUS_ENVERIFICATION = 3;
    CONST STATUS_VALIDE   = 4;
    CONST STATUS_LITIGE   = 5;
    CONST STATUS_NULL     = 6;
    CONST STATUS_BLANC    = 7;
    CONST STATUS_ROLLBACK = 8;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="string", length=10)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;
        
    /**
     * @ORM\ManyToOne(targetEntity="\my\Bundle\UserBundle\Entity\User", inversedBy="bulletins")
     * @ORM\JoinColumn(name="operateur_id", referencedColumnName="id")
     */
    private $operateur;

    /**
     * @var int
     *
     * @ORM\Column(name="validateur_id", type="integer", nullable=true)
     */
    private $validateur;
        
    /**
     * @ORM\OneToMany(targetEntity="History", mappedBy="user")
     */
    private $histories;  
        
    /**
     * @ORM\OneToMany(targetEntity="Vote", mappedBy="bulletin")
     */
    private $votes;

	/**
	 * @var string
	 * @ORM\Column(name="sexe",  type="text", length=45,nullable=true)
	 */
	private $sexe;
    
    function __construct() {
        $this->votes = new ArrayCollection();
        $this->histories = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Bulletin
     */
    public function setNumero(string $numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return int
     */
    public function getNumero(): string
    {
        return $this->numero;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Bulletin
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Bulletin
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Bulletin
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Bulletin
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Bulletin
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Get histories
     *
     * @return collection
     */
    function getHistories() {
        return $this->histories;
    }

    /**
     * Get votes
     *
     * @return collection
     */
    function getVotes() {
        return $this->votes;
    }

    /**
     * Set histories
     *
     * @param array $histories
     *
     * @return Bulletin
     */
    function setHistories($histories) {
        $this->histories = $histories;
        return $this;
    }

    /**
     * Set votes
     *
     * @param array $votes
     *
     * @return Bulletin
     */
    function setVotes($votes) {
        $this->votes = $votes;
        return $this;
    }

    /**
     * Add Vote
     *
     * @param Vote $vote
     *
     * @return Bulletin
     */
    public function addVote( Vote $vote )
    {
        $this->votes[] = $vote;

        return $this;
    }

    /**
     * Remove Vote
     *
     * @param Vote $vote
     */
    public function removeVote( Vote $vote )
    {
        $this->votes->removeElement( $vote );
    }

	/**
	 * Set sexe.
	 *
	 * @param string $sexe
	 *
	 * @return Bulletin
	 */
	public function setSexe( $sexe )
	{
		$this->sexe = $sexe;

		return $this;
	}

	/**
	 * Get sexe.
	 *
	 * @return string
	 */
	public function getSexe()
	{
		return $this->sexe;
	}

    /**
     * Add History
     *
     * @param History $history
     *
     * @return Bulletin
     */
    public function addHistory(History $history )
    {
        $this->histories[] = $history;

        return $this;
    }

    /**
     * Remove History
     *
     * @param History $history
     */
    public function removeHistory( History $history )
    {
        $this->histories->removeElement( $history );
    }
    
    /**
     * Get operateur
     *
     * @return User
     */
    function getOperateur() {
        return $this->operateur;
    }

    /**
     * Set operateur
     *
     * @param User $operateur
     *
     * @return Bulletin
     */
    function setOperateur(User $operateur) {
        $this->operateur = $operateur;
        return $this;
    }
    
    function getValidateur() {
        return $this->validateur;
    }

    function setValidateur($validateur) {
        $this->validateur = $validateur;
        return $this;
    }

    function getComment() {
        return $this->comment;
    }

    function setComment($comment) {
        $this->comment = $comment;
        return $this;
    }


}

