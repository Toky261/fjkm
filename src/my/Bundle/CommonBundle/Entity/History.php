<?php

namespace my\Bundle\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use my\Bundle\UserBundle\Entity\User;

/**
 * history
 *
 * @ORM\Table(name="history")
 * @ORM\Entity(repositoryClass="my\Bundle\CommonBundle\Repository\HistoryRepository")
 */
class History
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;
        
    /**
     * @ORM\ManyToOne(targetEntity="\my\Bundle\UserBundle\Entity\User", inversedBy="histories")
     * @ORM\JoinColumn(name="operateur_id", referencedColumnName="id")
     */
    private $operateur;

    /**
     * @var int
     *
     * @ORM\Column(name="validateur_id", type="integer", nullable=true)
     */
    private $validateur;
        
    /**
     * @ORM\ManyToOne(targetEntity="\my\Bundle\CommonBundle\Entity\Bulletin", inversedBy="histories")
     * @ORM\JoinColumn(name="bulletin_id", referencedColumnName="id")
     */
    private $bulletin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return history
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return history
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return history
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return history
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Get operateur
     *
     * @return User
     */
    function getOperateur() {
        return $this->operateur;
    }

    /**
     * Get bulletin
     *
     * @return Bulletin
     */
    function getBulletin() {
        return $this->bulletin;
    }

    /**
     * Set operateur
     *
     * @param User $operateur
     *
     * @return history
     */
    function setOperateur(User $operateur) {
        $this->operateur = $operateur;
        return $this;
    }

    /**
     * Set bulletin
     *
     * @param Bulletin $bulletin
     *
     * @return history
     */
    function setBulletin(Bulletin $bulletin) {
        $this->bulletin = $bulletin;
        return $this;
    }
    
    
    function getValidateur() {
        return $this->validateur;
    }

    function setValidateur($validateur) {
        $this->validateur = $validateur;
        return $this;
    }

}

