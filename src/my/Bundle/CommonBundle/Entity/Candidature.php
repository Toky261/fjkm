<?php

namespace my\Bundle\CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use my\Bundle\CommonBundle\Entity\Vote;
use my\Bundle\CommonBundle\Repository\CandidatureRepository;
use my\Bundle\UserBundle\Entity\User;

/**
 * Candidature
 *
 * @ORM\Table(name="candidature")
 * @ORM\Entity(repositoryClass="\my\Bundle\CommonBundle\Repository\CandidatureRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Candidature
{

	/**
	 * @var int
	 */
	const CONST_STATUS_ENABLED = 1;

	/**
	 * @var int
	 */
	const CONST_STATUS_DISABLED = 0;

	/**
	 * @var int
	 */
	const CONST_STATUS_DELETED = 10;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="nbVote", type="integer", nullable=true)
     */
    private $nbVote;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;
        
    /**
     * @ORM\ManyToOne(targetEntity="\my\Bundle\UserBundle\Entity\User", inversedBy="candidatures")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
        
    /**
     * @ORM\OneToMany(targetEntity="Vote", mappedBy="candidature")
     */
    private $votes;
    
    function __construct() {
        $this->votes = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbVote
     *
     * @param integer $nbVote
     *
     * @return Candidature
     */
    public function setNbVote($nbVote)
    {
        $this->nbVote = $nbVote;

        return $this;
    }

    /**
     * Get nbVote
     *
     * @return int
     */
    public function getNbVote()
    {
        return $this->nbVote;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Candidature
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Candidature
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Candidature
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get votes
     *
     * @return collection
     */
    function getVotes() {
        return $this->votes;
    }

    /**
     * Set votes
     *
     * @param array $votes
     *
     * @return Candidature
     */
    function setVotes($votes) {
        $this->votes = $votes;
        return $this;
    }

    /**
     * Add Vote
     *
     * @param Vote $vote
     *
     * @return Candidature
     */
    public function addVote( Vote $vote )
    {
        $this->votes[] = $vote;

        return $this;
    }

    /**
     * Remove Vote
     *
     * @param Vote $vote
     */
    public function removeVote( Vote $vote )
    {
        $this->votes->removeElement( $vote );
    }
    
    /**
     * Get user
     *
     * @return User
     */
    function getUser() {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Bulletin
     */
    function setUser($user) {
        $this->user = $user;
        return $this;
    }

	/**
	 * @ORM\PrePersist()
	 */
	public function prePersistEvent()
	{
		$this->setCreatedValue();
		$this->setUpdatedValue();
    }

	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdateEvent()
	{
		$this->setUpdatedValue();
	}
    
	private function setCreatedValue()
	{
		if (!$this->created) {
			$this->created = new \DateTime();
		}
    }

	private function setUpdatedValue()
	{
		if (!$this->updated) {
			$this->updated = new \DateTime();
		}
    }


}

