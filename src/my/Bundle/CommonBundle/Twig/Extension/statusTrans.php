<?php

namespace my\Bundle\CommonBundle\Twig\Extension;

use my\Bundle\CommonBundle\Entity\Bulletin;

use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

class statusTrans extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('statusTrans', [$this, 'translate']),
            new TwigFunction('statusBadge', [$this, 'badge']),
        ];
    }

    public function translate($status)
    {
        switch ($status) {
            case Bulletin::STATUS_INIT:
                $status = "Non attribué";
                break;
            case Bulletin::STATUS_ATTRIBUE:
                $status = "Attribué";
                break;
            case Bulletin::STATUS_AVALIDER:
                $status = "A valider";
                break;
            case Bulletin::STATUS_VALIDE:
                $status = "Validé";
                break;
            case Bulletin::STATUS_LITIGE:
                $status = "Litige";
                break;
            case Bulletin::STATUS_NULL:
                $status = "Null";
                break;
            case Bulletin::STATUS_BLANC:
                $status = "Blanc";
                break;
            case Bulletin::STATUS_ENVERIFICATION:
                $status = "En cours de vérification";
                break;
            case Bulletin::STATUS_ROLLBACK:
                $status = "Retour a l'opérateur";
                break;
            
            default:
                $status = "Non attribué";
                break;
        }

        return $input;
    }

    public function badge($status)
    {
        switch ($status) {
            case Bulletin::STATUS_INIT:
                $badge = "<span class='badge badge-default'>Non attribué</span>";
                break;
            case Bulletin::STATUS_ATTRIBUE:
                $badge = "<span class='badge badge-info'>Attribué</span>";
                break;
            case Bulletin::STATUS_AVALIDER:
                $badge = "<span class='badge badge-primary'>A valider</span>";
                break;
            case Bulletin::STATUS_VALIDE:
                $badge = "<span class='badge badge-success'>Validé</span>";
                break;
            case Bulletin::STATUS_LITIGE:
                $badge = "<span class='badge badge-warning'>Litige</span>";
                break;
            case Bulletin::STATUS_NULL:
                $badge = "<span class='badge badge-default'>Null</span>";
                break;
            case Bulletin::STATUS_BLANC:
                $badge = "<span class='badge badge-default'>Blanc</span>";
                break;
            case Bulletin::STATUS_ENVERIFICATION:
                $badge = "<span class='badge badge-default'>Vérification</span>";
                break;
            case Bulletin::STATUS_ROLLBACK:
                $badge = "<span class='badge badge-default'>Retour</span>";
                break;
            
            default:
                $badge = "<span class='badge badge-default'>Non attribué</span>";
                break;
        }

        return $badge;
    }
}