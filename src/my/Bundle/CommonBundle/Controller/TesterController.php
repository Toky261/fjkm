<?php

namespace my\Bundle\CommonBundle\Controller;

use my\Bundle\CommonBundle\Entity\Bulletin;
use my\Bundle\CommonBundle\Entity\Candidature;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Testeur controller.
 * @Route("/admin/tester")
 */
class TesterController extends Controller
{
    /**
     * @Route("/", name="tester_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $search = $request->request->get('search');
        $result = null;
        
        if(!empty($search)) {
            $bulletin = $em->getRepository(Bulletin::class)->find($search);

            if($bulletin != null) {
                $currentBulletin = $em->getRepository(Bulletin::class)->findByLitigeOrToValidate($bulletin->getId());

                // recuperation liste des candidats
                $candidats = $em->getRepository(Candidature::class)->getCandidatsBySexe($currentBulletin->getSexe());

                return $this->render('admin/tester/index.html.twig', [
                    'bulletin'  => $currentBulletin,
                    'candidats' => $candidats,
                ]);
            }

            $result = "Ballot not found";
        }

        return $this->render('admin/tester/index.html.twig', [
            'result'  => $result,
        ]);
    }

    /**
     * @Route("/view/{id}", name="tester_view")
	 * @Method("GET")
     */
    public function viewAction(Request $request, Bulletin $bulletin)
    {
        $em = $this->getDoctrine()->getManager();
        if ($bulletin) {
            $currentBulletin = $em->getRepository(Bulletin::class)->findByLitigeOrToValidate($bulletin->getId());

            // recuperation liste des candidats
            $candidats = $em->getRepository(Candidature::class)->getCandidatsBySexe($currentBulletin->getSexe());

            return $this->render('admin/tester/view.html.twig', [
                'bulletin'  => $currentBulletin,
                'candidats' => $candidats,
            ]);
        }
        else {
            return $this->redirectToRoute('tester_index');
        }
    }
}
