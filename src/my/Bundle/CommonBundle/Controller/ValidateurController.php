<?php

namespace my\Bundle\CommonBundle\Controller;

use my\Bundle\CommonBundle\Entity\Candidature;
use my\Bundle\CommonBundle\Entity\Vote;
use my\Bundle\CommonBundle\Form\CandidatureType;
use my\Bundle\CommonBundle\Entity\Bulletin;
use my\Bundle\CommonBundle\Entity\History;
use my\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Validateur controller.
 * @Route("admin/validateur")
 * @Security("has_role('ROLE_VALIDATEUR')")
 */
class ValidateurController extends Controller
{
	/**
	 * First screen from the Validateur.
	 * @Route("/", name="validateur_index")
	 * @Method("GET")
	 */
	public function indexAction()
	{
            $em = $this->getDoctrine()->getManager();

            $validateur = $this->getUser();

            $bulletin = $em->getRepository(Bulletin::class)->findOneBy([
                'validateur' => $validateur->getId(),
                'status'    => Bulletin::STATUS_ENVERIFICATION,
            ]);

            if (!$bulletin){

                /** @var Bulletin $currentBulletin */
                $bulletin = $em->getRepository(Bulletin::class)->findByLitigeOrToValidate();
            }

            if ($bulletin) {
                
                $operator = $bulletin->getOperateur();
                $litigationComment = $bulletin->getComment();
                
                // recuperation liste des candidats
                $candidats = $em->getRepository(Candidature::class)->getCandidatsBySexe($bulletin->getSexe());

                dump($bulletin);
                
                $dateNow = new \DateTime();
                $bulletin->setStatus(Bulletin::STATUS_ENVERIFICATION);
                $bulletin->setValidateur($validateur->getId());
                $bulletin->setUpdated($dateNow);
                $em->persist($bulletin);

                $history = new History();
                $history->setOperateur($operator);
                $history->setValidateur($validateur->getId());
                $history->setBulletin($bulletin);
                $history->setCreated($dateNow);
                $history->setUpdated($dateNow);
                $history->setStatus(Bulletin::STATUS_ENVERIFICATION);

                $em->persist($history);
                $em->flush();

                return $this->render('admin/validateur/index.html.twig', [
                    'bulletin'  => $bulletin,
                    'candidats' => $candidats,
                    'litigationComment' => $litigationComment,
                    'operator' => $operator,
                ]);
            }

            return $this->render('admin/validateur/404.html.twig');
	}

	/**
	 * @Route("/save/{id}/{isNull}", defaults={"isNull" = false}, name="validateur_save")
	 * @Method("POST")
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param \my\Bundle\CommonBundle\Entity\Bulletin   $bulletin
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 * @throws \Exception
	 */
	public function save(Request $request, Bulletin $bulletin, $isNull)
	{
            if ($bulletin) {            
                $validateur = $this->getUser();
                $em         = $this->getDoctrine()->getManager();
                $newStatus = $isNull ? Bulletin::STATUS_NULL : Bulletin::STATUS_VALIDE;
                $voteBlanc = false;
                $isRollback = false;

                if ($request->get('rollback')) {
                    $newStatus = Bulletin::STATUS_ROLLBACK;
                    $isRollback = true;
                    $candidatUserIds = [];
                    $operateur = $em->getRepository(User::class)->find($request->get('operateur_id'));
                    $bulletin->setOperateur($operateur);
                }
                else {
                    $candidatUserIds = json_decode($bulletin->getContent());
                }

                if (empty($candidatUserIds) && $isRollback == false && $newStatus != Bulletin::STATUS_NULL ) {
                    $newStatus = Bulletin::STATUS_BLANC;
                    $voteBlanc = true;
                }
                elseif ($newStatus != Bulletin::STATUS_NULL && count($candidatUserIds) > $this->container->getParameter('maxVote')) {
                    $newStatus = Bulletin::STATUS_NULL;
                }
                else {
                    if (!$isNull) {
                        dump($candidatUserIds);
                        foreach ($candidatUserIds as $candidatUserId) {

                            $candidat = $em->getRepository(Candidature::class)->findOneByUser($candidatUserId);
                            dump($candidatUserId);

                            /* @var $candidat Candidature */

                            // si pas encore voté pour un bulletin donné
                            if (!$em->getRepository(Vote::class)
                                    ->checkVote(['bulletin' => $bulletin, 'candidature' => $candidat])
                            ) {
                                $vote = new Vote();
                                $vote->setStatus(Vote::CONST_STATUS_ENABLED);
                                $vote->setBulletin($bulletin);
                                $vote->setCandidature($candidat);
                                $em->persist($vote);

                                $nbVote = $candidat->getNbVote() + 1 ;
                                $candidat->setNbVote($nbVote);
                                $em->persist($candidat);
                            }
                        }
                    }
                }            

                // Save votes History
                $dateNow = new \DateTime();
                $history = new History();
                $history->setOperateur($bulletin->getOperateur());
                $history->setValidateur($validateur->getId());
                $history->setBulletin($bulletin);
                $history->setCreated($dateNow);
                $history->setUpdated($dateNow);
                $history->setStatus($newStatus);
                $em->persist($history);

                // Update current ballot
                $bulletin->setStatus($newStatus);
                $bulletin->setUpdated($dateNow);
                $em->persist($bulletin);

                $em->flush();

                // Send message to User
                switch ($newStatus) {
                    case Bulletin::STATUS_ROLLBACK:
                        $flashMessage = 'Le présent bulletin (n° ' . $bulletin->getNumero() . ') a été remis à l\'opérateur avec succès';
                        break;
                    case Bulletin::STATUS_VALIDE:
                        $flashMessage = 'Les votes du bulletin n° ' . $bulletin->getNumero() . ' a été validé avec succès';
                        break;
                    case Bulletin::STATUS_NULL:
                        $flashMessage = 'Le présent bulletin (n° ' . $bulletin->getNumero() . ') a été enregistré comme vote nul';
                        break;
                    case Bulletin::STATUS_BLANC:
                        $flashMessage = 'Le présent bulletin (n° ' . $bulletin->getNumero() . ') a été enregistré comme vote blanc';
                        break;
                }

                $this->addFlash(
                    'success',
                    $flashMessage
                );
            }

            return $this->redirectToRoute('validateur_index');
	}
}
