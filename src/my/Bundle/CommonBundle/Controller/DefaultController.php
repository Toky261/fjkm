<?php

namespace my\Bundle\CommonBundle\Controller;

use my\Bundle\CommonBundle\Services\CsvParserService;
use my\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\KernelInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/import/membres")
     * 
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function indexAction(CsvParserService $oCsvParser, KernelInterface $kernel)
    {
    	$zFile = $kernel->getRootDir() . '/../oneshot/mpandray.csv';
    	if (file_exists($zFile)) {
		    $oCsvParser->delimiter = ';';
//		    $oCsvParser->encoding( 'UTF-8', 'WINDOWS-1252' );
		    $tCsv = $oCsvParser->parse_file($zFile);
		    /** @var \Doctrine\Common\Persistence\ManagerRegistry $oEm */
		    $oEm = $this->get('doctrine.orm.default_entity_manager');
//		    $oRepository = $oEm->getRepository('myUserBundle:user');
//		    $t = end($tCsv);
		    $oEncoder = $this->get('security.password_encoder');;
//		    dump($tCsv); die;
		    foreach ($tCsv as $k => $tItem) {
		    	if ($k > 2) {
		    		$tName = explode( ' ', $tItem[2]);
		    		$zUsername = strtolower($tItem["LAH."] . '_' . $tName[0]);
//		    		dump( $tName); die;
		    		$oUser = new User();
		    		$oUser->setUsername($zUsername);
		    		$oUser->setUsernameCanonical($zUsername);
		    		$oUser->setEmail($zUsername . '@mail.fjkm');
		    		$oUser->setEmailCanonical($zUsername . '@mail.fjkm');
		    		$oUser->setEnabled(1);
		    		$oUser->setPassword($oEncoder->encodePassword($oUser, $zUsername));
		    		$oUser->setRoles( ['ROLE_USER'] );
				    $oUser->setLastName( $tName[0]);

				    unset( $tName[0]);
				    $zFirstname = implode( ' ', $tName);
				    
				    $oUser->setFirstName( $zFirstname);
				    $oUser->setPhone( $tItem[5]);
				    $oUser->setAdresse( $tItem[3]);
				    $oUser->setSector( $tItem[4]);
				    $oUser->setUserType( User::USER_TYPE_EGLISE);

				    $oUser->setNewRegistryNumber($tItem["LAH."]);
				    $oUser->setRegistryNumber($tItem[1]);
				    $oUser->setRegistryNumber($tItem[1]);

				    $oEm->persist($oUser);
				    $oEm->flush($oUser);
			    }
		    }
	    }

	    echo 'Import OK';
        return $this->render('myCommonBundle:Default:index.html.twig');
    }
}
