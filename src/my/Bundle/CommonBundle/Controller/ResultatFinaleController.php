<?php

namespace my\Bundle\CommonBundle\Controller;

use my\Bundle\CommonBundle\Entity\Candidature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Resultat Finale controller.
 * @Route("resultat/finale")
 */
class ResultatFinaleController extends Controller
{
    /**
     * @Route("/", name="final_result_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $filterCivility = $request->request->get('civility');
        $civility       = ($filterCivility != null) ? $filterCivility : 'Homme';
        
        $candidats = $em->getRepository(Candidature::class)->getFinalResult($civility, $this->container->getParameter('maxVote'));

        return $this->render('public/resultat/finale/index.html.twig', [
            'candidats' => $candidats,
            'civility' => $civility,
        ]);
    }
}
