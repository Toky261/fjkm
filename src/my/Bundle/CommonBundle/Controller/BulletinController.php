<?php

namespace my\Bundle\CommonBundle\Controller;

use my\Bundle\UserBundle\Entity\User;
use my\Bundle\CommonBundle\Entity\History;
use my\Bundle\CommonBundle\Entity\Bulletin;
use my\Bundle\CommonBundle\Form\BulletinEditType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Bulletin controller.
 *
 * @Route("admin/bulletin")
 */
class BulletinController extends Controller
{
    /**
     * @Route("/", name="bulletin_index")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $operators = $em->getRepository(User::class)->findByRole('ROLE_OPERATEUR');

        $form = $this->createForm(BulletinEditType::class, null, [
                'operators' => $operators,
            ]
        );

        return $this->render('admin/bulletin/index.html.twig', [
            'bulletins' => $em->getRepository(Bulletin::class)->findAll(),
            'form'      => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bulletin_edit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request, Bulletin $bulletin)
    {
        if ($bulletin) {
            $em = $this->getDoctrine()->getManager();
            $admin = $this->getUser();
            $newStatus = Bulletin::STATUS_ATTRIBUE;
            $operatorId = (int)$request->request->get('bulletin_edit')['operator'];
            $operator   = $em->getRepository(User::class)->find($operatorId);

            // Save assignment History (Admin action)
            $dateNow = new \DateTime();
            $historyEdit = new History();
            $historyEdit->setOperateur($operator);
            $historyEdit->setValidateur($admin->getId());
            $historyEdit->setBulletin($bulletin);
            $historyEdit->setCreated($dateNow);
            $historyEdit->setUpdated($dateNow);
            $historyEdit->setStatus($newStatus);
            $historyEdit->setComment('Attribut current bulletin to userId ' . $operatorId);
            $em->persist($historyEdit);

            // Update current ballot
            $bulletin->setOperateur($operator);
            $bulletin->setValidateur($admin->getId());
            $bulletin->setContent('');
            $bulletin->setStatus($newStatus);
            $bulletin->setUpdated($dateNow);
            $em->persist($bulletin);

            $em->flush();

            $this->addFlash(
                'success',
                'Attribution du bulletin n° ' . $bulletin->getId() . ' avec succès'
            );
        }

        return $this->redirectToRoute('bulletin_index');
    }

    public function bulletinStatistics()
    {
        $em = $this->getDoctrine()->getManager();
            
        return $this->render('admin/Bulletin/statistic.html.twig', [
            'stat' => $em->getRepository(Bulletin::class)->getBulletinStatistics(),
        ]);
    }
}
