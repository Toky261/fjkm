<?php

namespace my\Bundle\CommonBundle\Controller;

use my\Bundle\CommonBundle\Entity\Candidature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Resultat Provisoire controller.
 * @Route("resultat/provisoire")
 */
class ResultaProvisoireController extends Controller
{
    /**
     * @Route("/", name="provisional_result_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $filterCivility = $request->request->get('civility');
        $civility       = ($filterCivility != null) ? $filterCivility : 'Femme';
        
        $candidats = $em->getRepository(Candidature::class)->getCandidatsBySexe($civility);

        return $this->render('public/resultat/provisoire/index.html.twig', [
            'candidats' => $candidats,
            'civility' => $civility,
        ]);
    }

}
