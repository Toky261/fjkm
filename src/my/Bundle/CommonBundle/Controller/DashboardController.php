<?php

namespace my\Bundle\CommonBundle\Controller;

use my\Bundle\UserBundle\Entity\User;
use my\Bundle\CommonBundle\Entity\Vote;
use my\Bundle\CommonBundle\Entity\History;
use my\Bundle\CommonBundle\Entity\Bulletin;
use my\Bundle\CommonBundle\Entity\Candidature;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Dashboard controller.
 *
 * @Route("admin/dashboard")
* @Security("has_role('ROLE_ADMIN')")
 */
class DashboardController extends Controller
{
    /**
	 * @Route("/", name="dashboard")
	 * @Method("GET")
	 */
	public function indexAction()
	{
        $em = $em = $this->getDoctrine()->getManager();

        return $this->render('admin/dashboard/index.html.twig', [
            'histories' => $em->getRepository(History::class)->findRecent(10),
            'votes' => $em->getRepository(Bulletin::class)->getTotalVote(),
        ]);
	}
}
