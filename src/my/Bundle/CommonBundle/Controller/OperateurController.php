<?php

namespace my\Bundle\CommonBundle\Controller;

use my\Bundle\CommonBundle\Entity\Candidature;
use my\Bundle\CommonBundle\Entity\Vote;
use my\Bundle\CommonBundle\Form\CandidatureType;
use my\Bundle\CommonBundle\Entity\Bulletin;
use my\Bundle\CommonBundle\Entity\History;
use my\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Candidature controller.
 * @Route("admin/operateur")
 * @Security("has_role('ROLE_OPERATEUR')")
 */
class OperateurController extends Controller
{
	/**
	 * First screen from the operateur.
	 * @Route("/", name="operateur_index")
	 * @Method("GET")
	 */
	public function indexAction()
	{
            $em = $this->getDoctrine()->getManager();

            $operateur = $this->getUser();

            /** @var Bulletin $currentBulletin */
            $currentBulletin = $em->getRepository(Bulletin::class)->findByStatusRollBackOrAttributed($operateur->getId());

            if ($currentBulletin != null) {
                
                // recuperation liste des candidats
                $candidats = $em->getRepository(Candidature::class)->getCandidatsBySexe($currentBulletin->getSexe());

                return $this->render('admin/operateur/index.html.twig', [
                    'bulletin'  => $currentBulletin,
                    'candidats' => $candidats,
                ]);
            }

            $bulletin = $em->getRepository(Bulletin::class)->findOneByStatus(Bulletin::STATUS_INIT);

            if (!$bulletin) {
                return $this->render('admin/operateur/404.html.twig');
            }

            $dateNow = new \DateTime();
            $bulletin->setStatus(Bulletin::STATUS_ATTRIBUE);
            $bulletin->setOperateur($operateur);
            $bulletin->setUpdated($dateNow);
            $em->persist($bulletin);

            $history = new History();
            $history->setOperateur($operateur);
            $history->setBulletin($bulletin);
            $history->setCreated($dateNow);
            $history->setUpdated($dateNow);
            $history->setStatus(Bulletin::STATUS_ATTRIBUE);

            $em->persist($history);
            $em->flush();

            // recuperation liste des candidats
            $candidats = $em->getRepository(Candidature::class)->getCandidatsBySexe($bulletin->getSexe());

            return $this->render('admin/operateur/new.html.twig');
	}

	/**
	 * @Route("/save/{id}/{isLitigation}", defaults={"isLitigation" = false}, name="operateur_save")
	 * @Method("POST")
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param \my\Bundle\CommonBundle\Entity\Bulletin   $bulletin
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 * @throws \Exception
	 */
	public function save(Request $request, Bulletin $bulletin, $isLitigation)
	{
            if ($bulletin) {
                $operateur = $this->getUser();
                $em        = $this->getDoctrine()->getManager();
                $candidatUserIds = $request->request->get('candidat');
                $newStatus = $isLitigation ? Bulletin::STATUS_LITIGE : Bulletin::STATUS_AVALIDER;

                // Insert History
                $dateNow = new \DateTime();
                $history = new History();
                $history->setOperateur($operateur);
                $history->setBulletin($bulletin);
                $history->setCreated($dateNow);
                $history->setUpdated($dateNow);
                $history->setStatus($newStatus);
                if ($isLitigation){
                    $history->setComment($request->request->get('litigation'));
                    $bulletin->setComment($request->request->get('litigation'));
                }
                    
                $em->persist($history);

                // Update current ballot
                $bulletin->setContent(json_encode($candidatUserIds));
                $bulletin->setStatus($newStatus);
                $bulletin->setUpdated($dateNow);
                $em->persist($bulletin);

                $em->flush();

                // Send message to User
                $flashMessage = $isLitigation ? 'Le litige du bulletin n° ' . $bulletin->getNumero() . ' a été enregistré avec succès' : 'Les votes du bulletin n° ' . $bulletin->getNumero() . ' a été enregistré avec succès';
                $this->addFlash(
                    'success',
                    $flashMessage
                );
            }

            return $this->redirectToRoute('operateur_index');
	}
}
