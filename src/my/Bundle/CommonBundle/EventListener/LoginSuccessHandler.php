<?php
/**
 * Description of LoginListener.php.
 *
 * @package my\Bundle\CommonBundle\EventListener
 * @author  Joelio
 */

namespace my\Bundle\CommonBundle\EventListener;

use App\Entity\LoginHistory;

/**
 * Class LoginListener
 *
 * @package my\Bundle\CommonBundle\EventListener
 */

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{

	protected $router;

	protected $authorizationChecker;

	public function __construct(Router $router, AuthorizationChecker $authorizationChecker)
	{
		$this->router               = $router;
		$this->authorizationChecker = $authorizationChecker;
	}

	public function onAuthenticationSuccess(Request $request, TokenInterface $token)
	{

		$response = null;

		if ($this->authorizationChecker->isGranted('ROLE_OPERATEUR')) {
			$response = new RedirectResponse($this->router->generate('operateur_index'));
		} else if ($this->authorizationChecker->isGranted('ROLE_VALIDATEUR')) {
			$response = new RedirectResponse($this->router->generate('validateur_index'));
		} else if ($this->authorizationChecker->isGranted('ROLE_CONTROLLEUR')) {
			$response = new RedirectResponse($this->router->generate('accueil'));
		} else if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
			$response = new RedirectResponse($this->router->generate('admin_dashboard'));
		} else if ($this->authorizationChecker->isGranted('ROLE_USER')) {
			$response = new RedirectResponse($this->router->generate('accueil'));
		}

		return $response;
	}

}