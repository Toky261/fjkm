<?php

namespace my\Bundle\CommonBundle\Form;

use Doctrine\ORM\EntityRepository;
use my\Bundle\CommonBundle\Entity\Candidature;
use my\Bundle\UserBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CandidatureType extends AbstractType
{
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('nbVote')
			->add(
				'status',
				ChoiceType::class,
				[
					"choices"     =>
						[
							"Actif"   => Candidature::CONST_STATUS_ENABLED,
							"Inactif" => Candidature::CONST_STATUS_DISABLED
						],
				]
			)
			->add('user', EntityType::class,
			      [
				      'class'        => User::class,
				      'query_builder' => function (EntityRepository $er) {
					      return $er->createQueryBuilder('u')
					                ->orderBy('u.lastName', 'ASC');
				      },
				      'choice_label' => function ($user)
				      {
					      return $user->getLastName() . ' ' . $user->getFirstName();
				      }
			      ]
			)
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			                       'data_class' => 'my\Bundle\CommonBundle\Entity\Candidature'
		                       ));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix()
	{
		return 'my_bundle_commonbundle_candidature';
	}


}
