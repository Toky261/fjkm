<?php

namespace my\Bundle\SampanaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SampanaBureauType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('created', DateTimeType::class, ["label" => "Date création"])
            ->add('status', ChoiceType::class, ["choices" => ["Actif" => 1, "Inactif" => 0], "placeholder" => "Choisir status"])
            ->add('sampana')
            ->add("user")
            ->add('bureau');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'my\Bundle\SampanaBundle\Entity\SampanaBureau'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'my_bundle_sampanabundle_sampanabureau';
    }


}
