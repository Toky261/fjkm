<?php

namespace my\Bundle\SampanaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType ;
use Symfony\Component\Form\Extension\Core\Type\TextareaType ;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\Request; 
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FileType; 
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class SampanaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('attr'=>array('label'=>'Nom : ','placeholder'=>'Nom')))
                ->add('code',null,array('attr'=>array('label'=>'code : ','placeholder'=>'code')))
                ->add('logo',FileType::class, array('label' => 'Inseret un logo','data_class'=>null,
                 'required'=>false))
                ->add('slogan',null,array('attr'=>array('label'=>'slogan : ','placeholder'=>'slogan')))
                ->add('status', ChoiceType::class, ["choices" => ["Actif" => 1, "Inactif" => 0], "placeholder" => "choix du status"]);
                
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'my\Bundle\SampanaBundle\Entity\Sampana'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'my_bundle_sampanabundle_sampana';
    }


}
