<?php
/**
 * Created by PhpStorm.
 * User: JESS
 * Date: 12/11/2018
 * Time: 18:31
 */

namespace my\Bundle\SampanaBundle\Controller;


use my\Bundle\SampanaBundle\Entity\Bureau;
use my\Bundle\SampanaBundle\Form\BureauType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BureauController
 * @package my\Bundle\SampanaBundle\Controller
 * @Route("/bureau")
 */
class BureauController extends Controller
{
    /**
     * @Route("/", name="bureau_index")
     * @return Response
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $items =$em->getRepository(Bureau::class)->findAll();
        return $this->render("admin/bureau/index.html.twig", ["items" => $items]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/new", name="bureau_new")
     */
    public function new(Request $request)
    {
        $bureau = new Bureau();
        $form = $this->createForm(BureauType::class, $bureau);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bureau);
            $em->flush();
            $this->addFlash("success", "Enregistrement effectué avec succès");
            
            return $this->redirectToRoute("bureau_index");
        }
        
        return $this->render("admin/bureau/new.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @param Bureau $bureau
     * @return Response
     * @Route("/show/{id}", name="bureau_show")
     */
    public function show(Bureau $bureau)
    {
        return $this->render("admin/bureau/show.html.twig", ["item" => $bureau]);
    }

    /**
     * delete bureau from database
     * @param Bureau $bureau
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @Route("/delete/{id}", name="bureau_delete")
     */
    public function delete(Bureau $bureau)
    {
        if ($bureau != null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bureau);
            $em->flush();
            $this->addFlash("success", "Suppression effectuée avec succès");    
            
            return $this->redirectToRoute("bureau_index");
        }

        return $this->createNotFoundException("Bureau introuvable");
    }

    /**
     * @param Bureau $bureau
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * edit bureau
     * @Route("/edit/{id}", name="bureau_edit")
     */
    public function edit(Bureau $bureau, Request $request)
    {
        $form =$this->createForm(BureauType::class, $bureau);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash("success", "Modification effectuée avec succès");
            
            return $this->redirectToRoute("bureau_index");
        }

        return $this->render("admin/bureau/edit.html.twig", ["form" => $form->createView(), "item" => $bureau]);
    }
}