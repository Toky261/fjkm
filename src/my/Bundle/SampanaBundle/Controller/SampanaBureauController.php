<?php
/**
 * Created by PhpStorm.
 * User: JESS
 * Date: 13/11/2018
 * Time: 10:01
 */

namespace my\Bundle\SampanaBundle\Controller;


use my\Bundle\SampanaBundle\Entity\Sampana;
use my\Bundle\SampanaBundle\Entity\SampanaBureau;
use my\Bundle\SampanaBundle\Form\SampanaBureauType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SampanaBureauController
 * @package my\Bundle\SampanaBundle\Controller
 * @Route("/sampana-bureau")
 */
class SampanaBureauController extends Controller
{
    /**
     * list all sampana-bureau
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="sampana-bureau_index")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $items = $em->getRepository(SampanaBureau::class)->findAll();

        return $this->render("admin/sampana-bureau/index.html.twig", ["items" => $items]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/new", name="sampana-bureau_new")
     */
    public function new(Request $request)
    {
        $sb = new SampanaBureau();
        $form = $this->createForm(SampanaBureauType::class, $sb);
        $form->handleRequest($request);

        if  ($form->isValid() && $form->isValid()) {
            if (!$this->isExist($sb)){
                $em = $this->getDoctrine()->getManager();
                $em->persist($sb);
                $em->flush();
                $this->addFlash("success", "Enregistrement effectué avec succès");

                return $this->redirectToRoute("sampana-bureau_index");
            } else {
                $this->addFlash("error", "L'enregistement existe déjà");
            }
        }

        return $this->render("admin/sampana-bureau/new.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @param Request $request
     * @param SampanaBureau $sampanaBureau
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/edit/{id}", name="sampana-bureau_edit")
     */
    public function edit(Request $request, SampanaBureau $sampanaBureau)
    {
        $form = $this->createForm(SampanaBureauType::class, $sampanaBureau);
        $form->handleRequest($request);

        if ($form->isSubmitted()&& $form->isValid()) {
            if (!$this->isExist($sampanaBureau)) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $this->addFlash("success", "Modification effectuée avec succès");

                return $this->redirectToRoute("sampana-bureau_index");
            } else {
                $this->addFlash("error", "L'enregistrement existe déjà");
            }
        }

        return $this->render("admin/sampana-bureau/edit.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @param SampanaBureau $sampanaBureau
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @Route("/show/{id}", name="sampana-bureau_show")
     */
    public function show(SampanaBureau $sampanaBureau)
    {
        if ($sampanaBureau) {
            return $this->render("admin/sampana-bureau/show.html.twig", ["item" => $sampanaBureau]);
        }

        return $this->createNotFoundException("Sampana bureau introuvable");
    }

    /**
     * @param SampanaBureau $sampanaBureau
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/delete/{id}", name="sampana-bureau_delete")
     */
    public function delete(SampanaBureau $sampanaBureau)
    {
        if ($sampanaBureau){
            $em = $this->getDoctrine()->getManager();
            $em->remove($sampanaBureau);
            $em->flush();
            $this->addFlash("success", "Suppression effectuée avec succès");

            return $this->redirectToRoute("sampana-bureau_index");
        }
        
        return $this->createNotFoundHttpException("Sampana bureau introuvable");
    }

    /**
     * @param SampanaBureau $sampanaBureau
     * @return bool
     * check if sampanabureau already exists
     */
    private function isExist(SampanaBureau $sampanaBureau)
    {
        $em = $this->getDoctrine()->getManager();
        $found = $em->getRepository(SampanaBureau::class)->findOneBy(["sampana" => $sampanaBureau->getSampana(), "bureau" => $sampanaBureau->getBureau()]);
        if ($found && $found->getId() != $sampanaBureau->getId()) {
            return true;
        }

        return false;
    }
}