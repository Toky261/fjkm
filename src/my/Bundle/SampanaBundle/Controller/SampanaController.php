<?php

namespace my\Bundle\SampanaBundle\Controller;

use my\Bundle\SampanaBundle\Entity\Sampana;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\File;
use my\Bundle\SampanaBundle\Form\SampanaType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

//https://symfony.com/doc/3.4/controller/upload_file.html

/**
 * Sampana controller.
 *
 * @Route("/admin/sampana")
 * @Security("has_role('ROLE_ADMIN')")
 */
class SampanaController extends Controller
{
    /**
     * Lists all sampana entities.
     *
     * @Route("/", name="admin_sampana_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sampanas = $em->getRepository('mySampanaBundle:Sampana')->findAll();

        return  array(
            'sampanas' => $sampanas,
        );
    }

    /**
     * Creates a new sampana entity.
     *
     * @Route("/new", name="admin_sampana_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $sampana = new Sampana();
        $form = $this->createForm('my\Bundle\SampanaBundle\Form\SampanaType', $sampana);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            // $file stores the uploaded image file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $sampana->getLogo();
            
            if ($file){
                $fileName = self::normalizeString($sampana->getName()).'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('logo_sampana'),
                        $fileName
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                    throw new \Exception($e);
                }

                // instead of its contents
                $sampana->setLogo($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($sampana);
            $em->flush();
            $this->addFlash("success", "Enregistrement effectué avec succès");

            return $this->redirectToRoute('admin_sampana_index', array('id' => $sampana->getId()));
        }
        
        return array(
            'sampana' => $sampana,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a sampana entity.
     *
     * @Route("/{id}", name="sampana_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Sampana $sampana)
    {
        $deleteForm = $this->createDeleteForm($sampana);

        return  array(
            'sampana' => $sampana,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing sampana entity.
     *
     * @Route("/{id}/edit", name="admin_sampana_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function edit(Request $request, Sampana $sampana)
    {
        $deleteForm = $this->createDeleteForm($sampana);
        $Form = $this->createForm(SampanaType::class, $sampana);
        $logoFile = $sampana->getLogo();
        $Form->handleRequest($request);


        if (is_file($this->getParameter('logo_sampana').'/'.$sampana->getLogo())) {
            $sampana->setLogo(
                new File($this->getParameter('logo_sampana').'/'.$sampana->getLogo())
            );
        }


        if ($Form->isSubmitted() && $Form->isValid()) {
            $this->getDoctrine()->getManager()->flush();




            return $this->redirectToRoute('admin_sampana_edit', array('id' => $sampana->getId()));
        }

        return  array(
            'logoFile' => $logoFile,
            'sampana' => $sampana,
            'form' => $Form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a sampana entity.
     *
     * @Route("/{id}/delete", name="admin_sampana_delete")
     * @Template()
     */
    public function deleteAction(Request $request, Sampana $sampana)
    {
        if ($sampana) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sampana);
            $em->flush();
            $this->addFlash("success", "Suppression effectuée avec succès");

        return $this->redirectToRoute('admin_sampana_index');
        }

        return $this->createNotFoundHttpException(" Sampana introuvable");
    }


    /**
     * Creates a form to delete a sampana entity.
     *
     * @param Sampana $sampana The sampana entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sampana $sampana)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_sampana_delete', array('id' => $sampana->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    public static function normalizeString ($str = '')
    {
        $str = strip_tags($str); 
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
        $str = str_replace(' ', '-', $str);
        $str = rawurlencode($str);
        $str = str_replace('%', '-', $str);
        return $str;
    }
    
    

    /**
     * Edits an existing Sampana document.
     *
     * @Route("/{id}/update", name="sampana_update")
     * @Method("POST")
     * @Template("mySampanaBundle:Sampana:edit.html.twig")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request, $id)
    {
        $dm = $this->getDoctrine()->getManager();

        $document = $dm->getRepository('mySampanaBundle:Sampana')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Sampana document.');
        }
        
        $directory = $this->getUploadSampanaDirectory();
        
        if (is_file($directory . $document->getLogo())) {
            $logo = new File($directory . $document->getLogo());
            $document->setLogo($logo);
        }  

        $deleteForm = $this->createDeleteForm($document);
        $editForm   = $this->createForm(SampanaType::class, $document);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            
            $directory = $this->getUploadSampanaDirectory();

            $file = $editForm['logo']->getData();
            if ($file){
                $fileName = self::normalizeString($editForm['name']->getData());
                $extension = $file->guessExtension();
                if (!$extension) {
                    // extension cannot be guessed
                    $extension = 'jpg';
                }
                $newName = $fileName . '.' . $extension;
                $file->move($directory, $fileName . '.' . $extension);

                $document->setLogo($newName) ;
            }
                
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_sampana_index'));
        }

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    
    

    /**
     * Returns the UploadSampanaDirectory
     *
     * @return string
     */
    private function getUploadSampanaDirectory()
    {
        return $this->getParameter('logo_sampana');
    }
}
