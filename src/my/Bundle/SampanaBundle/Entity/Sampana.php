<?php

namespace my\Bundle\SampanaBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use my\Bundle\SampanaBundle\Entity\SampanaBureau;
use DateTime;

/**
 * Sampana
 *
 * @ORM\Table(name="sampana")
 * @ORM\Entity(repositoryClass="my\Bundle\SampanaBundle\Repository\SampanaRepository")
 */
class Sampana
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=45, nullable=true)
     */
    private $code;

    /**
     * @var logo
     * @ORM\Column(name="logo", type="string", length=45, nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="text", nullable=true)
     */
    private $slogan;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="SampanaBureau", mappedBy="sampana")
     * 
     */
    private $sampana_bureaux;
   /**
    * @var Collection
    * @ORM\ManyToMany(targetEntity="my\Bundle\UserBundle\Entity\User", mappedBy="sampana")
    *
    */
    private $users;
    /**
     * @var Datetime
     * @ORM\Column(name="date_creation", type="datetime", length=15, nullable=true)
     */
    private $dateCreation;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sampana
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Sampana
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Sampana
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return Sampana
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Sampana
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sampana_bureaux = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sampanaBureaux
     *
     * @param \my\Bundle\SampanaBundle\Entity\SampanaBureau $sampanaBureaux
     *
     * @return Sampana
     */
    public function addSampanaBureaux(\my\Bundle\SampanaBundle\Entity\SampanaBureau $sampanaBureaux)
    {
        $this->sampana_bureaux[] = $sampanaBureaux;

        return $this;
    }

    /**
     * Remove sampanaBureaux
     *
     * @param \my\Bundle\SampanaBundle\Entity\SampanaBureau $sampanaBureaux
     */
    public function removeSampanaBureaux(\my\Bundle\SampanaBundle\Entity\SampanaBureau $sampanaBureaux)
    {
        $this->sampana_bureaux->removeElement($sampanaBureaux);
    }

    /**
     * Get sampanaBureaux
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSampanaBureaux()
    {
        return $this->sampana_bureaux;
    }


    
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add user
     *
     * @param \my\Bundle\UserBundle\Entity\User $user
     *
     * @return Sampana
     */
    public function addUser(\my\Bundle\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \my\Bundle\UserBundle\Entity\User $user
     */
    public function removeUser(\my\Bundle\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
    /**
     * @ORM\PrePersist
     */
    public function enable()
    {

        $this->setEnabled(true);
    }
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function uploadImage()
    {
        $file = $this->getLogo();
        if ($file instanceof UploadedFile) {
            $fileNom = md5(uniqid()).'.'.$file->guessExtension();
            try {
                $file->move(
                    'logo/sampana',
                    $fileNom
                );

                $this->setLogo($fileNom);
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
                throw new \Exception($e);
            }
        }
    }

    /**
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param DateTime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

}
