<?php

namespace my\Bundle\SampanaBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use my\Bundle\SampanaBundle\Entity\SampanaBureau;

/**
 * Bureau
 *
 * @ORM\Table(name="bureau")
 * @ORM\Entity(repositoryClass="my\Bundle\SampanaBundle\Repository\BureauRepository")
 */
class Bureau
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="SampanaBureau", mappedBy="bureau")
     */
    private $bureau_sampana;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bureau
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bureau_sampana = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bureauSampana
     *
     * @param SampanaBureau $bureauSampana
     *
     * @return Bureau
     */
    public function addBureauSampana(SampanaBureau $bureauSampana)
    {
        $this->bureau_sampana[] = $bureauSampana;

        return $this;
    }

    /**
     * Remove bureauSampana
     *
     * @param SampanaBureau $bureauSampana
     */
    public function removeBureauSampana(SampanaBureau $bureauSampana)
    {
        $this->bureau_sampana->removeElement($bureauSampana);
    }

    /**
     * Get bureauSampana
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBureauSampana()
    {
        return $this->bureau_sampana;
    }

    /**
     * @return string
     * get string
     */
    public function __toString()
    {
        return $this->name;
    }
}
