<?php

namespace my\Bundle\SampanaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use my\Bundle\SampanaBundle\Entity\Bureau;
use my\Bundle\SampanaBundle\Entity\Sampana;

/**
 * SampanaBureau
 *
 * @ORM\Table(name="sampana_bureau")
 * @ORM\Entity(repositoryClass="my\Bundle\SampanaBundle\Repository\SampanaBureauRepository")
 * @ORM\HasLifecycleCallbacks
 */
class SampanaBureau
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var Sampana
     * @ORM\ManyToOne(targetEntity="Sampana", inversedBy="sampana_bureaux")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $sampana;

    /**
     * @var Bureau
     * @ORM\ManyToOne(targetEntity="Bureau", inversedBy="bureau_sampana")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $bureau;

    /**
     * @ORM\ManyToOne(targetEntity="my\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SampanaBureau
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return SampanaBureau
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set sampana
     *
     * @param Sampana $sampana
     *
     * @return SampanaBureau
     */
    public function setSampana(Sampana $sampana = null)
    {
        $this->sampana = $sampana;

        return $this;
    }

    /**
     * Get sampana
     *
     * @return Sampana
     */
    public function getSampana()
    {
        return $this->sampana;
    }

    /**
     * Set bureau
     *
     * @param Bureau $bureau
     *
     * @return SampanaBureau
     */
    public function setBureau(Bureau $bureau = null)
    {
        $this->bureau = $bureau;

        return $this;
    }

    /**
     * Get bureau
     *
     * @return Bureau
     */
    public function getBureau()
    {
        return $this->bureau;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setDateCreation()
    {
        $this->setCreated(new \DateTime("now"));
    }

    /**
     * Set user
     *
     * @param \my\Bundle\UserBundle\Entity\User $user
     *
     * @return SampanaBureau
     */
    public function setUser(\my\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \my\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
