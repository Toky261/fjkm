<?php

namespace my\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType ;
use Symfony\Component\Form\Extension\Core\Type\TextareaType ;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class UserType extends AbstractType
{
     /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $builder->add('userName', null, array('attr' => array('label' => 'userName : ', 'placeholder' => 'pseudo')))
	            ->add('plainpassword', RepeatedType::class, array(
		            'type'           => PasswordType::class,
		            'required'       => false,
		            'first_options'  => array('label' => 'Entrer votre mot de passe'),
		            'second_options' => array('label' => 'Confirmer votre mot de passe'),
	            ))
	            ->add('sampana', null, array('attr' => array('label' => 'sampana : ', 'placeholder' => 'sampana')))
	            ->add('lastName', null, array('attr' => array('label' => 'lastName : ', 'placeholder' => 'Nom')))
	            ->add('firstName', null, array('attr' => array('label' => 'firstName : ', 'placeholder' => 'Prenoms')))
	            ->add('email', null, array('attr' => array('label' => 'email : ', 'placeholder' => 'email')))
	            ->add('birthday', null,
	                  array('attr' => array('label' => 'birthday : ', 'placeholder' => 'Date de votre anniversaire')))
	            ->add('image', FileType::class, array(
		            'label'      => 'Photo',
		            'data_class' => null,
		            'required'   => false
	            ))
	            ->add('sexe', ChoiceType::class, array(
		            'choices'    => array(
			            'Homme ♂' => 'Homme',
			            'Femme ♀' => 'Femme'
		            ),
		            'required'   => false,
		            'empty_data' => null
	            ))
	            ->add(
		            'profil',
		            ChoiceType::class,
		            array(
			            'choices' => array(
				            '---' => '',
				            'Administrateur' => 'ROLE_ADMIN',
				            'Utilisateur'    => 'ROLE_USER',
				            'Operateur'      => 'ROLE_OPERATEUR',
				            'Valaidateur'    => 'ROLE_VALIDATEUR',
				            'Testeur'        => 'ROLE_TESTEUR',
			            ),
			            'mapped'  => false,
			            'required'  => false
		            )
	            )
	            ->add('adresse', null, array('attr' => array('label' => 'adresse : ', 'placeholder' => 'adresse')))
	            ->add('phone', null,
	                  array('attr' => array('label' => 'phone : ', 'placeholder' => 'Numero de votre Telephone')))
	            ->add('status', ChoiceType::class,
	                  ["choices" => ["Actif" => 1, "Inactif" => 0], "placeholder" => "choix du status"])
	    ;

    }
        /**
        * {@inheritdoc}
        */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'my\Bundle\UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'my_bundle_userbundle_user';
    }


}
