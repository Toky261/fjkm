<?php

namespace my\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class myUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
