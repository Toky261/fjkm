<?php

namespace my\Bundle\UserBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as FosUser;
use my\Bundle\CommonBundle\Entity\Candidature;
use my\Bundle\CommonBundle\Entity\History;
use my\Bundle\SampanaBundle\Entity\Sampana;
use my\Bundle\UserBundle\Repository\UserRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * User
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="\my\Bundle\UserBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends FosUser
{

	/**
	 * @vat int
	 */
	const USER_TYPE_USER = 1;

	/**
	 * @var int
	 */
	const USER_TYPE_EGLISE = 2;

	/**
	 * @var int
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;


	/**
	 * @var string
	 * @ORM\Column(name="first_name", type="string", nullable=true)
	 */

	protected $firstName;

	/**
	 * @var string
	 * @ORM\Column(name="last_name", type="string", length=45, nullable=true)
	 */

	protected $lastName;

	/**
	 * @var string
	 * @ORM\Column(name="phone", type="string", length=45, nullable=true)
	 */

	protected $phone;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="birthday", type="string", nullable=true)
	 */
	protected $birthday;

	/**
	 * @var image
	 * @ORM\Column(name="image", type="string", length=45, nullable=true)
	 */
	protected $image;

	/**
	 * @var string
	 * @ORM\Column(name="sexe",  type="text", length=45,nullable=true)
	 */
	protected $sexe;

	/**
	 * @var string
	 * @ORM\Column(name="adresse",  type="string", length=255, nullable=true)
	 */
	protected $adresse;

	/**
	 * @var Datetime
	 * @ORM\Column(name="date_creation", type="datetime", length=15, nullable=true)
	 */
	private $dateCreation;

	/**
	 * @var Sampana
	 * @ORM\ManyToMany(targetEntity="\my\Bundle\SampanaBundle\Entity\Sampana", inversedBy="users")
	 */
	private $sampana;

	/**
	 * @var integer
	 * @ORM\Column(name="registry_number", type="string", nullable=true)
	 */
	private $registryNumber;

	/**
	 * @var integer
	 * @ORM\Column(name="new_registry_number", type="string", nullable=true)
	 */
	private $newRegistryNumber;

	/**
	 * @var string
	 * @ORM\Column(name="sector", type="string", nullable=true)
	 */
	private $sector;

	/**
	 * @var int
	 * @ORM\Column(name="status", type="boolean", nullable=true)
	 */
	private $status;

	/**
	 * @var integer
	 * @ORM\Column(name="user_type", type="string", nullable=true)
	 */
	private $userType = self::USER_TYPE_USER;
        
        /**
         * @ORM\OneToMany(targetEntity="\my\Bundle\CommonBundle\Entity\Candidature", mappedBy="user")
         */
        private $candidatures;    
        
        /**
         * @ORM\OneToMany(targetEntity="\my\Bundle\CommonBundle\Entity\History", mappedBy="user")
         */
	private $histories; 
          
        
        /**
         * @ORM\OneToMany(targetEntity="\my\Bundle\CommonBundle\Entity\Bulletin", mappedBy="user")
         */
	private $bulletins; 
        
		
	private $nbVote;
    
        function __construct() {
            $this->histories = new ArrayCollection();
            $this->bulletins = new ArrayCollection();
            $this->candidatures = new ArrayCollection();
        }     

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get firstName.
	 *
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}


	/**
	 * Set firstName.
	 *
	 * @param string $firstName
	 *
	 * @return User
	 */
	public function setFirstName( $firstName )
	{
		$this->firstName = $firstName;

		return $this;
	}

	/**
	 * Get lastName.
	 *
	 * @return string
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Set lastName.
	 *
	 * @param string $lastName
	 *
	 * @return User
	 */
	public function setLastName( $lastName )
	{
		$this->lastName = $lastName;

		return $this;
	}

	/**
	 * Get phone.
	 *
	 * @return string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * Set phone.
	 *
	 * @param string $phone
	 *
	 * @return User
	 */
	public function setPhone( $phone )
	{
		$this->phone = $phone;

		return $this;
	}

	/**
	 * Set birthday.
	 *
	 * @param string $birthday
	 *
	 * @return User
	 */
	public function setBirthday( $birthday )
	{
		$this->birthday = $birthday;

		return $this;
	}

	/**
	 * Get birthday.
	 *
	 * @return string
	 */
	public function getBirthday()
	{
		return $this->birthday;
	}

	/**
	 * Set image.
	 *
	 * @param string $image
	 *
	 * @return User
	 */
	public function setImage( $image )
	{
		$this->image = $image;

		return $this;
	}

	/**
	 * Get image.
	 *
	 * @return string
	 */
	public function getImage()
	{
		return $this->image;
	}


	/**
	 * Set sexe.
	 *
	 * @param string $sexe
	 *
	 * @return User
	 */
	public function setSexe( $sexe )
	{
		$this->sexe = $sexe;

		return $this;
	}

	/**
	 * Get sexe.
	 *
	 * @return string
	 */
	public function getSexe()
	{
		return $this->sexe;
	}

	/**
	 * Set adresse.
	 *
	 * @param string $adresse
	 *
	 * @return User
	 */
	public function setAdresse( $adresse )
	{
		$this->adresse = $adresse;

		return $this;
	}

	/**
	 * Get adresse.
	 *
	 * @return string
	 */
	public function getAdresse()
	{
		return $this->adresse;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function enable()
	{

		$this->setEnabled( true );
	}

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function uploadImage()
	{
            $file = $this->getImage();
            if ( $file instanceof UploadedFile ) {
                    $fileNom = md5( uniqid() ) . '.' . $file->guessExtension();
                    try {
                            $file->move(
                                    'images/user',
                                    $fileNom
                            );

                            $this->setImage( $fileNom );
                    } catch ( FileException $e ) {
                            // ... handle exception if something happens during file upload
                            throw new \Exception( $e );
                    }
            }
	}

	/**
	 * @return DateTime
	 */
	public function getDateCreation()
	{
		return $this->dateCreation;
	}

	/**
	 * @param DateTime $dateCreation
	 */
	public function setDateCreation( $dateCreation )
	{
		$this->dateCreation = $dateCreation;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function setDateToNow()
	{
		$this->setDateCreation( new DateTime( "now" ) );
	}

	/**
	 * @return Sampana
	 */
	public function getSampana()
	{
		return $this->sampana;
	}

	/**
	 * @param Sampana $sampana
	 *
	 * @return User
	 */
	public function setSampana( $sampana )
	{
		$this->sampana = $sampana;

		return $this;
	}

	/**
	 * Add sampana
	 *
	 * @param Sampana $sampana
	 *
	 * @return User
	 */
	public function addSampana( Sampana $sampana )
	{
		$this->sampana[] = $sampana;

		return $this;
	}

	/**
	 * Remove sampana
	 *
	 * @param Sampana $sampana
	 */
	public function removeSampana( Sampana $sampana )
	{
		$this->sampana->removeElement( $sampana );
	}

	/**
	 * Set status
	 *
	 * @param integer $status
	 *
	 * @return User
	 */
	public function setStatus( $status )
	{
		$this->status = $status;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getRegistryNumber()
	{
		return $this->registryNumber;
	}

	/**
	 * @param int $registryNumber
	 */
	public function setRegistryNumber( $registryNumber )
	{
		$this->registryNumber = $registryNumber;
	}

	/**
	 * @return int
	 */
	public function getNewRegistryNumber()
	{
		return $this->newRegistryNumber;
	}

	/**
	 * @param int $newRegistryNumber
	 */
	public function setNewRegistryNumber( $newRegistryNumber )
	{
		$this->newRegistryNumber = $newRegistryNumber;
	}

	/**
	 * @return string
	 */
	public function getSector()
	{
		return $this->sector;
	}

	/**
	 * @param string $sector
	 */
	public function setSector( $sector )
	{
		$this->sector = $sector;
	}

	/**
	 * @return int
	 */
	public function getUserType()
	{
		return $this->userType;
	}

	/**
	 * @param int $userType
	 */
	public function setUserType( $userType )
	{
		$this->userType = $userType;
	}

	/**
	 * Get status
	 *
	 * @return integer
	 */
	public function getStatus()
	{
		// if ($status == )
		return $this->status;
	}

	/**
	 * Get nbVote.
	 */
	public function getNbVote() : int
	{
		return $this->nbVote;
	}

	/**
	 * Set nbVote.
	 */
	public function setNbVote(int $nbVote) : int
	{
		$this->nbVote = $nbVote;
		
		return $this;
	}
}
