<?php

namespace my\Bundle\UserBundle\Controller;

use my\Bundle\UserBundle\Entity\User;
use my\Bundle\UserBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;



/**
 * User controller.
 *
 * @Route("admin/user")
 */
class UserController extends Controller
{
    /**
     * s all user entities.
     *
     * @Route("/lisitry-ny-mpandray", name="admin_user_index")
     * @Method("GET")
     * @Template()
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('myUserBundle:User')->findAll();
        return  array(
            'users' => $users,
        );
    }

    public function adminDashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // or add an optional message - seen by developers
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
    }
    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('my\Bundle\UserBundle\Form\UserType', $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

	        $role     = $form->get('profil')->getData();
	        $user->setRoles([$role]);

	        if (!$user->getUsername()) {
	        	$user->setUsername($user->getEmail());
	        }
	        
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash("success", "Enregistrement effectué avec succès");

            return $this->redirectToRoute('admin_user_index');

        }

        return  array(
            'user' => $user,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     * @Template()
     */
    public function show(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function edit(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $Form = $this->createForm(UserType::class, $user);

	    $roles = $user->getRoles();
	    if (!empty($roles[0])) {
		    $Form->get('profil')->setData($roles[0]);
	    }

        $image = $user->getImage();
        $Form->handleRequest($request);

        if ($Form->isSubmitted() && $Form->isValid()) {
            
            if (!empty($user->getPlainPassword())) {
                $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
            }
	        $role     = $Form->get('profil')->getData();
	        $user->setRoles(array($role));

            $em = $this->getDoctrine()->getManager();
            if ($user->getImage()== null || $user->getImage()== '') {
                $user->setImage($image);
            }

            $user->setEnabled($user->getStatus());

            $em->persist($user);
            $em->flush();
            $this->addFlash("success", "Modification effectuée avec succès");

            return $this->redirectToRoute('admin_user_index');

        }

        return  array(
            'user' => $user,
            'form' => $Form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @param user $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/delete/{id}", name="user_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function delete(Request $request, User $user)
    {
        if ($user){
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $this->addFlash("success", "Suppression effectuée avec succès");


            return $this->redirectToRoute('admin_user_index');
         }

        return $this->createNotFoundHttpException(" User introuvable");
    }
    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    public static function normalizeString ($str = '')
    {
        $str = strip_tags($str); 
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
        $str = str_replace(' ', '-', $str);
        $str = rawurlencode($str);
        $str = str_replace('%', '-', $str);
        return $str;
    }
    
     /**
     * Edits an existing user document.
     *
     * @Route("/{id}/update", name="user_update")
     * @Method("POST")
     * @Template("myuserBundle:user:edit.html.twig")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request, User $user)
    {
        $dm = $this->getDoctrine()->getManager();
       // $document = $dm->getRepository('myUserBundle:user')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find user document.');
        }

        $deleteForm = $this->createDeleteForm($user);
        $editForm   = $this->createForm(UserType::class, $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_user_index'));
        }

        return array(
            'document'    => $user,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    
    

    /**
     * Returns the UploaduserDirectory
     *
     * @return string
     */
    private function getUploaduserDirectory()
    {
        return $this->getParameter('images_directory');
    }
    
}
