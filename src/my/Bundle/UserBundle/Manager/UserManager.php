<?php
/**
 * Description of UserManager.php.
 *
 * @package my\Bundle\UserBundle\Manager
 * @author  Joelio
 */

namespace my\Bundle\UserBundle\Manager;


use Doctrine\ORM\EntityManager;
use my\Bundle\CommonBundle\Manager\BaseManager;

class UserManager extends BaseManager
{
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	protected $em;

	/**
	 * @var \Symfony\Component\DependencyInjection\ContainerInterface
	 */
	protected $container;

	/**
	 * @var \Symfony\Component\HttpFoundation\Session\Session
	 */
	protected $session;

	/**
	 * @var \Symfony\Component\Form\FormFactory
	 */
	protected $formFactory;

	/**
	 * @var null|\Symfony\Component\HttpFoundation\Request
	 */
	protected $request;

	public function __construct(EntityManager $em, ContainerInterface $container, Session $session, FormFactory $formFacory, RequestStack $requestStack)
	{
		$this->em          = $em;
		$this->container   = $container;
		$this->session     = $session;
		$this->formFactory = $formFacory;
		$this->request     = $requestStack->getCurrentRequest();
	}

	public function getRepository()
	{
		return $this->em->getRepository('myUserBundle:User');
	}
	
	public function search(array $tCriteria)
	{
		$query = $this->getRepository()->findByQueryBuilder($tCriteria);

		return $query->getQuery()->getResult();
	}
}