<?php

namespace my\Bundle\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
	/**
	 * @Route("/", name="accueil")
	 * @param \Symfony\Component\HttpFoundation\Request $oRequest
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction(Request $oRequest)
	{
		$em = $this->getDoctrine()->getManager();

		$zSearch = $oRequest->request->get('search', '');
		$zSearch = trim($zSearch);
		$toUser = (!empty($zSearch)) ? $em->getRepository('myUserBundle:User')->findByQueryBuilder(['zSearch' => $zSearch]) : [];

		return $this->render(
			'myFrontBundle:Default:index.html.twig',
			[
				'toUser'  => $toUser,
				'zSearch' => $zSearch,
			]
		);
	}
}
