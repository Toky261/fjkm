<?php
/**
 * Description of SearhType.php.
 *
 * @package my\Bundle\FrontBundle\Form
 * @author  Joelio
 */

namespace my\Bundle\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearhType extends AbstractType
{

	/**
	 * @param \Symfony\Component\Form\FormBuilderInterface $oBuilder
	 * @param array                                        $tOptions
	 */
	public function buildForm(FormBuilderInterface $oBuilder, array $tOptions)
	{
		$oBuilder->add(
			'search',
			TextType::class,
			[
				'mapped' => false,
			]
		);
	}

	/**
	 * @param \Symfony\Component\OptionsResolver\OptionsResolver $oResolver
	 */
	public function configureOptions(OptionsResolver $oResolver)
	{
		$oResolver->setDefaults(
			[
				'data_class'         => 'myUserBundle\Entity\User',
				'csrf_protection'    => false,
				"allow_extra_fields" => true,
			]
		);
	}
}