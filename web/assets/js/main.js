function notif(message, type) {
    $.notify({
        // options
        icon: '',
        title: '',
        message: message,
        url: '',
        target: ''
    }, {
        // settings
        element: 'body',
        type: type,
        allow_dismiss: true,
        placement: {
            from: "bottom",
            align: "right"
        },
        animate: {
            enter: 'animated bounceIn',
            exit: 'animated bounceOut'
        }
    });
}
